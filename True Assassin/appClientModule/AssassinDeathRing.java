/*
 * Class in which the actual game will be played this will have the following methods
 * 
	
	$Deletes a player based on 
	public void deleteAssassin(String codeName)

 * 
 * 
 */

public class AssassinDeathRing {
	protected AssassinNode first;
	protected AssassinNode last;
	public int assassinCount=0;//count of players left in the game
	
	//Generic constructor
	public AssassinDeathRing() {
		first=null;
		last=null;
	}
	
	//checks to see if their are any player in the game
	public boolean isEmpty(){
		return assassinCount==0;
	}//isEmpty
	
	
	//returns the amount of players left in the game
	public int getPlayerCount(){
		return assassinCount;
	}//getPlayerCount
	
	//adds a player to the front of the linked list
	public void addPlayer (Assassin a){
	        AssassinNode temp = new AssassinNode(a, null, null);    
	        if (first == null){  //if no items in the list          
	            temp.changeTarget(temp);
	            temp.changeReaper(temp);
	            first = temp;
	            last = first;            
	        }
	        else{
	            temp.changeReaper(last);
	            last.changeTarget(temp);
	            first.changeReaper(temp);
	            temp.changeTarget(first);
	            last = temp;        
	        }
	       assassinCount++ ;
	    }//addPlayerInFront
	
	
	 
	
	//deletes a player based on 
	public void deleteAssassin(String codeName){
		//code if target to be deleted is at the end of the list
		   if (codeName == first.getPlayerName()){
	            first = first.getTarget();
	            last.changeTarget(first);
	            first.changeReaper(last);
	            assassinCount--;
	            return;
	        }
		 //code if target to be deleted is at the end of the list
		   if (codeName == last.getPlayerName()){
	            last = last.getReaper();
	            last.changeTarget(first);
	            first.changeReaper(last);
	            assassinCount--;
	            return;
	        }
		   //code used for anyone in the middle of the list
	        AssassinNode temp = first.getTarget(); 
	        for (int i = 2; i <= assassinCount; i++){
	            if (codeName == temp.getPlayerName()){
	                temp.getTarget().changeReaper(temp.getReaper());
	                temp.getReaper().changeTarget(temp.getTarget());
	                assassinCount-- ;
	                return;
	            }
	            temp = temp.getTarget();
	        }    
	        return;
	}
	
	//gets a target based on the assassin
	public String getTargetName(Assassin a){
		int count=0;
		AssassinNode temp=first;
		while (temp.getPlayer() != a){
			count ++;
			if (count==assassinCount){
				return "Player Does Not Exist";
			}
			temp=temp.getTarget();
		}//while
		
		return temp.getTargetName();
	}//getTarget
	
	//gets a target based on the codeName of the Assassin
	public String getTargetName(String codeName){
		int count=0;
		AssassinNode temp=first;
		while (temp.getPlayerName() != codeName){
			count ++;
			if (count==assassinCount){
				return "Player Does Not Exist";
			}
			temp=temp.getTarget();
		}//while
		
		return temp.getTargetName();
	}
	
	//gets a target based on the assassin
		public String getReaperName(Assassin a){
			AssassinNode temp=first;
			int count=0;
			while (temp.getPlayer() != a){
				count ++;
				if (count==assassinCount){
					return "Player Does Not Exist";
				}
				temp=temp.getTarget();
			}//while
			
			return temp.getReaperName();
		}
		
		//gets a target based on the codeName of the Assassin
		public String getReaperName(String codeName){
			AssassinNode temp=first;
			int count=0;
			while (temp.getPlayerName() != codeName){
				count ++;
				if (count==assassinCount){
					return "Player Does Not Exist";
				}
				temp=temp.getTarget();
			}//while
			
			return temp.getReaperName();

		}
}

