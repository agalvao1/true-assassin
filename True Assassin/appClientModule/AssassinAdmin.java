//admin class to be used to create assassin list order and to create the actual
//list to be used in the game
import java.util.ArrayList;
import java.util.Random;


public class AssassinAdmin {

	public ArrayList<Assassin> hitList= new ArrayList<Assassin>();
	
	public AssassinAdmin() {
		hitList=new ArrayList<Assassin>(20);
	}
	
	//method to be implemented that will randomly sort the list to be 
		//used in the game
		//-Isaac + Kenedi, we figured that this method would work well.
		//We also thought that if it was an account, like literally 
		//the entire persons account, we would have to sort differently
		//Let me know later on today, ill come in to work and see you
		//and you can probably give us other stuff to program too.
		public void RandomSort(){
			Random rand = new Random();

			int g;
			
			for(int i = 0; i < hitList.size()/2; i++)
			{
				g = rand.nextInt(hitList.size());
				Assassin temp = hitList.get(g); 
				hitList.set(g,hitList.get(i));
				hitList.set(i,temp);
				
			}
			
		}
	
	public int listCount(){
		return hitList.size();
	}

}
