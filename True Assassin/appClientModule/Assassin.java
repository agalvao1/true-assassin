import java.awt.image.BufferedImage;
import java.util.Random;

public class Assassin {
	private String codeName;// users codename chosen by them
	private BufferedImage picture;// users picture to be used during game
	private String userGameCode; // users code to be given to admin to recognize
									// player
	private int rannum;

	public Assassin() {
		codeName = null;
		picture = null;
		userGameCode = null;
	}

	public Assassin(String name) {
		codeName = name;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String CodeName) {
		codeName = CodeName;
	}

	public BufferedImage getPicture() {
		return picture;
	}

	public void setPicture(BufferedImage picture) {
		this.picture = picture;
	}

	public String getUserGameCode() {
		return userGameCode;
	}

	public void setUserGameCode() {
		int counter = 1; 			//While loop
		Random rand = new Random();	//Randomness
		int rannum;					//For value to be added to userGameCode
		int pick;					//For letter or number
		String temp;				//To add number to string
		
		while(counter <= 5)
		{
			pick = rand.nextInt(1) + 1;
			
			if(pick == 1)
			{
				rannum = rand.nextInt(26) + 97;
			
				userGameCode = userGameCode + Character.toString((char) rannum); //ascii to character
				
			}
			else
			{
				rannum = rand.nextInt(9) + 1;
				
				temp = "" + rannum;
				
				userGameCode = userGameCode + temp;
			}
			counter++;
		}
	}

}
