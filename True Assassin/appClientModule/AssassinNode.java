/*
 * Class in which each player will be given their assignment based on the next node/target 
 * they have been assigned based on the singly circular linked list
 */

public class AssassinNode {
	
	protected Assassin player;//assassins account of the node
	protected AssassinNode target;//target this user will be after
	protected AssassinNode reaper;//person trying to kill target
	
	public AssassinNode() {
		target=null;
		reaper=null;
		player=new Assassin();
	}
	
	public AssassinNode(Assassin p){
		player=p;
		target=null;
		reaper=null;
	}
	
	public AssassinNode(Assassin p,AssassinNode prev,AssassinNode next){
		player=p;
		target=next;
		reaper=prev;
	}
	
	public void changeReaper(AssassinNode newReaper){
		reaper=newReaper;
	}
	
	public void setPlayer(Assassin p){
		player=p;
	}
	
	public void changeTarget(AssassinNode newTarget){
		target = newTarget;
	}
	
	public Assassin getPlayer(){
		return player;
	}
	
	public AssassinNode getTarget(){
		return target;
	}

	public AssassinNode getReaper(){
		return reaper;
	}
	
	public String getPlayerName(){
		return player.getCodeName();
	}
	
	public String getTargetName(){
		return target.getPlayer().getCodeName();
	}
	
	public String getReaperName(){
		return reaper.getPlayer().getCodeName();
	}
}
